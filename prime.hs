import Data.List
prime =  [y | y <- [2..], factors y]
factors n = null[x | x <- [2..(floor.sqrt.fromIntegral) n], n `mod` x == 0]
main = print (prime!!10000)