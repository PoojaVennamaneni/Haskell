import Data.Char
factorial :: Integer -> Integer
factorial 0 = 1
factorial n | n > 0 = n * factorial(n-1)
digitsRes  = sum(map digitToInt x)
    where x = show(factorial 100)
main = print(digitsRes)