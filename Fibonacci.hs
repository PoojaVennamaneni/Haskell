import Data.List
import System.IO
fiblist = 0 : 1 :(zipWith(+)fiblist(tail fiblist))
fib = sum(takeWhile(<4000000)[x | x <- fiblist,rem x 2 == 0]) 