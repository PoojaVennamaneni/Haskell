import Data.List
import System.IO
isPrime :: Integer->Bool
isPrime i = null [y | y <- [2..(floor.sqrt.fromIntegral) i],i `mod` y == 0]
generatePrimes = sum(takeWhile(<2000000)[i | i <- [2.. ] , isPrime i])