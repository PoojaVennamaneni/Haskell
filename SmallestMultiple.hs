import Data.List
import System.IO
main :: IO ()
main = print . foldl1 lcm $ [1..20]