import System.IO
import Data.Char
--factorial :: Integer -> Integer
factorial 0 = 1
factorial n | n > 0 = n * factorial(n-1)
factors y = map(factorial).map(fromIntegral.digitToInt).show  $ y
factorSum x = sum $ factors x
result = sum $ [x | x <- [3..100000],factorSum x == x]
main= do
	print $ result