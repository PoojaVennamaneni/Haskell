import System.IO
import Data.List
main = product .head .rightTriangles $ 1000
rightTriangles  n = product[(a,b,c) | c <- [1..n],b <- [1..c],a <- [1..b], a^2 + b^2 == c^2, a+b+c == 1000] 
