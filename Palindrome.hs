import Data.List
import System.IO
reverseInt :: Integer -> Integer
reverseInt = read . reverse . show
palindrome =  maximum[x *y| x <- [100..999], y <- [100..999],let z = x * y, z ==reverseInt z]