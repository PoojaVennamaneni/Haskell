import Data.List
import System.IO
main = do print $ factors(nth 1000)
isPrime :: Integer->Bool
isPrime k = null [y | y <- [2..(floor.sqrt.fromIntegral) k], k `mod` y == 0]
factors n =  [x | x <- [1.. n], n `mod` x == 0,isPrime x]
