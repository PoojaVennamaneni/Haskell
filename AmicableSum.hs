import System.IO
import Data.List
main = putStrLn (show ans)
aliquote :: Int -> Int
amicable :: Int -> Bool
ans = sum[y | y <- [1..10000],amicable y]
amicable n = let m = aliquote n in (m /= n) && (aliquote m) == n
aliquote z = sum[x | x <- [1..z`div`2],(mod z x) == 0]