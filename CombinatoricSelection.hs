import Data.Char
import System.IO
factorial 0 = 1
factorial n | n > 0 = n * factorial(n-1)
combination(r,n) = factorial n `div` (factorial r * factorial (n-r))
temp = [(n,x) | x<-[1..100], n<-[1..x]]
res = length $ filter (>1000000) $ map combination (temp)
main= do
	print(res)