import Data.List
import System.IO
fiblist = 0:1:(zipWith(+)fiblist(tail fiblist))
fib =  length $ takeWhile (<10^999) fiblist
main = print(fib)